import angular from 'angular';
import directive from './countryDirective';

export default angular
    .module('country.module', [])
    .directive(directive.name, directive);

