import './module';
import angular from 'angular';

angular.element(document).ready(function () {
   // angular.bootstrap(document, ['claimSpiffWebApp.module'], {strictDi: true});
    angular.bootstrap(document, ['claimSpiffWebApp.module']);
});